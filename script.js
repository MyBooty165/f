function checkLink() {
  const url = document.getElementById("link").value;
  fetch(url)
    .then(response => {
      if (response.ok) {
        document.getElementById("result").innerHTML = "Link is accessible.";
      } else {
        document.getElementById("result").innerHTML = "Link is blocked.";
      }
    })
    .catch(error => {
      document.getElementById("result").innerHTML = "Error: " + error;
    });
}
